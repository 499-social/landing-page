##
# Mixer
#
# @file
# @version 0.1

entrypoints = src/index.html
public-url ?= /

all: dist

.PHONY: install
install:
	@rm -rf .installed
	@make .installed

.installed: package.json package-lock.json
	@echo "Dependencies files are newer than .installed; (re)installing."
	@npm clean-install
	@echo "This file is used by 'make' for keeping track of last install time. If package.json, package-lock.json or elm.json are newer then this file (.installed) then all 'make *' commands that depend on '.installed' know they need to run npm install first." \
		> .installed

.PHONY: dist
dist: .installed
	npx parcel build --public-url=$(public-url) $(entrypoints)

.PHONY: develop
develop: .installed
	npx parcel --public-url=$(public-url) $(entrypoints)

clean-artifacts:
	rm -rf \
		dist/ \
		.cache/ \
		elm-stuff/ \

# end
